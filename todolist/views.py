from django.shortcuts import render 
from django.shortcuts import redirect
import pyrebase 

# Configuration du projet firebase avec pyrebase.
config = {
    'apiKey': "AIzaSyA9_8VeV6BlFqMd_rQqoQ5SbYMzzapw1MY",
    'authDomain': "todolist-10079.firebaseapp.com",
    'databaseURL': "https://todolist-10079.firebaseio.com",
    'projectId': "todolist-10079",
    'storageBucket': "todolist-10079.appspot.com",
    'messagingSenderId': "139237989568",
    'appId': "1:139237989568:web:bfc389c07833be94a0a125",
    'measurementId': "G-LEH42BC4BW"   
}

firebase = pyrebase.initialize_app(config)
# Définition des utilisateurs du projet firebase.
auth = firebase.auth()
# Définition de la base de données.
db = firebase.database()

# Méthode pour l'affichage du formulaire d'inscription.
def signIn(request):
    return render(request, "signIn.html")

# Méthode pour l'authentification avec pyrebase.
def postsign(request):
    email=request.POST.get('email')
    passw = request.POST.get("pass")
    try:
        user = auth.sign_in_with_email_and_password(email,passw)
    except:
        message = "invalid cerediantials"
        return render(request,"signIn.html",{"msg":message})
    print(user)
    request.session['auth'] = True
    request.session['user'] = email

    return redirect("/home/")

# Méthode pour l'ajout d'une tache dans le tableau to_do_list de la base de donnée lors de la soumission du formulaire.
def addElement(request):

    element = request.POST.get('element')
    
    db.child("to_do_list").push(element)
    
    return redirect("/home/")

# Méthode pour afficher la to do list.
def home(request):
    liste = db.child("to_do_list").get()
    elements = []
    for element in liste.each():
        elements.append(element.val())

    # Vérification avec la session si l'utilisateur s'est authentifié.
    try:
        request.session['auth']
    except:
        return redirect("/")
    
    email = request.session['user']

    return render(request, "welcome.html", {"e": email, "list": elements})

